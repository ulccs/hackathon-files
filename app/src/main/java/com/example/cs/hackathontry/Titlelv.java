package com.example.cs.hackathontry;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class Titlelv extends AppCompatActivity {
ListView listView2;
    String[] storytitle = {"The Hare and The Tortoise","The Little Red Riding Hood","Snow White and the Seven Dwarfs","Cinderella"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_titlelv);
        listView2 = (ListView) findViewById(R.id.listView2);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,storytitle);
        listView2.setAdapter(adapter);
        listView2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(Titlelv.this,InsertandRetrieveBlobData.class);
                startActivity(i);
            }
        });
    }
}
