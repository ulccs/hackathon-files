package com.example.cs.hackathontry;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Rodel Villena on 11/25/2017.
 */
public class Adapter {
    public static final String KEY_ROWID = "_id";
    public static final String KEY_TITLE = "title";
    public static final String KEY_STORY = "story";


    private static final String TAG = "Adaptertwo";
    private DatabaseHelper mDbHelper;
    private SQLiteDatabase mDb;

    private static final String DATABASE_NAME = "Kiddiey";
    private static final String SQLITE_TABLE = "Data";
    private static final int DATABASE_VERSION = 1;


    private final Context mCtx;

    private static final String DATABASE_CREATE =
            "CREATE TABLE if not exists " + SQLITE_TABLE + " (" +
                    KEY_ROWID + " integer PRIMARY KEY autoincrement," +
                    //KEY_CODE + "," +
                    KEY_TITLE + "," +
                    // KEY_CLASSIFICATION + "," +
                    KEY_STORY + "," +

                    " UNIQUE (" + KEY_TITLE + "));";

    private static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }


        @Override
        public void onCreate(SQLiteDatabase db) {
            Log.w(TAG, DATABASE_CREATE);
            db.execSQL(DATABASE_CREATE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS " + SQLITE_TABLE);
            onCreate(db);
        }
    }

    public Adapter(Context ctx) {
        this.mCtx = ctx;
    }

    public Adapter open() throws SQLException {
        mDbHelper = new DatabaseHelper(mCtx);
        mDb = mDbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        if (mDbHelper != null) {
            mDbHelper.close();
        }
    }

    public long createData(String title,
                           String story) {

        ContentValues initialValues = new ContentValues();
        //initialValues.put(KEY_CODE, code);
        initialValues.put(KEY_TITLE, title);
        // initialValues.put(KEY_CLASSIFICATION, classification);
        initialValues.put(KEY_STORY, story);


        return mDb.insert(SQLITE_TABLE, null, initialValues);
    }

    public boolean deleteAllData() {

        int doneDelete = 0;
        doneDelete = mDb.delete(SQLITE_TABLE, null, null);
        Log.w(TAG, Integer.toString(doneDelete));
        return doneDelete > 0;

    }


    public Cursor fetchDataByWord(String inputText) throws SQLException {
        Log.w(TAG, inputText);
        Cursor mCursor = null;
        if (inputText == null || inputText.length() == 0) {
            mCursor = mDb.query(SQLITE_TABLE, new String[]{KEY_ROWID,
                            //KEY_CODE,
                            KEY_TITLE, KEY_STORY},
                    null, null, null, null, null);

        } else {
            mCursor = mDb.query(true, SQLITE_TABLE, new String[]{KEY_ROWID,
                            //KEY_CODE,
                            KEY_TITLE, KEY_STORY},
                    KEY_TITLE + " like '%" + inputText + "%'", null,
                    null, null, null, null);
        }
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;

    }

    public Cursor fetchAllData() {

        Cursor mCursor = mDb.query(SQLITE_TABLE, new String[]{KEY_ROWID,

                        //KEY_CODE,
                        KEY_TITLE,KEY_STORY},
                null, null, null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }
    public void insertSomeData(){
    createData("a","b");
        createData("c","d");
    }

}
