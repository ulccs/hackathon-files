package com.example.rinacabansag.secondtry;

import android.graphics.Bitmap;

/**
 * Created by Rina Cabansag on 25/11/2017.
 */
public class Employee {
    private Bitmap bmp;
    private String name;
    private int age;

    public Employee(Bitmap b) {
        bmp = b;

    }

    public Bitmap getBitmap() {
        return bmp;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

}

